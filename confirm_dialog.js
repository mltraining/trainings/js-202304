let dialogContainerElement = null

async function deleteRecord() {
  if (await displayDialog('Oled kindel, et soovid kirje kustutada?')) {
    console.log('record deleted')
  } else {
    console.log('record not deleted')
  }
}

async function displayDialog(message) {
  if (dialogContainerElement !== null) {
    return Promise.reject()
  }
  dialogContainerElement = document.createElement('div')
  dialogContainerElement.innerHTML = getDialogTemplate(message)
  document.body.appendChild(dialogContainerElement)

  return new Promise((resolve, reject) => {
    document.querySelector('.yes-button').addEventListener('click', () => {
      closeDialog()
      resolve(true)
    })
    document.querySelector('.no-button').addEventListener('click', () => {
      closeDialog()
      resolve(false)
    })
  })
}

function closeDialog() {
  document.body.removeChild(dialogContainerElement)
  dialogContainerElement = null
}

function yesButtonClicked() {
  document.body.removeChild(dialogContainerElement)
  dialogContainerElement = null
}

function noButtonClicked() {
  document.body.removeChild(dialogContainerElement)
  dialogContainerElement = null
}

const getDialogTemplate = (question) => /*html*/ `
      <div class="content-cover"></div>
      <div class="dialog-box">
          <div class="queston-row">
          ${question}
          </div>
          <div class="button-row">
              <button class="yes-button">
                  Jah
              </button>
              <button class="no-button">
                  Ei
              </button>
          </div>
      </div>
  `

//   const getDialogTemplate = (question) => /*html*/ `
//         <div class="content-cover"></div>
//         <div class="dialog-box">
//             <div class="queston-row">
//             ${question}
//             </div>
//             <div class="button-row">
//                 <button class="yes-button" onClick="yesButtonClicked()">
//                     Jah
//                 </button>
//                 <button class="no-button" onClick="noButtonClicked()">
//                     Ei
//                 </button>
//             </div>
//         </div>
//     `

//   displayDialog('Kas oled kindel, et soovid teada, mis on elu mõte?')
