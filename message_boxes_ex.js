// --------------------------------------------
// ----- Ülesanne 1 (position: fixed)
// --------------------------------------------
// Tekita veebilehele helesinist värvi kast
// mõõtmetega 300px X 200px ning aseta see
// ekraani alumisest servast 50 piksli
// ning paremast servast 30 piksli kaugusele.
// Fikseeri element sellele asukohale.
// Kirjuta element füüsiliselt HTML dokumendi sisse.
// Vihje: style="background: lightblue; position: fixed",

// --------------------------------------------
// ----- Ülesanne 2 (position: absolute)
// --------------------------------------------
// Tekita veebilehele tumesinist värvi kast
// mõõtmetega 250px X 150px ning aseta see
// ekraani ülemisest servast 10 piksli
// ning paremast servast 20 piksli kaugusele.
// Kasuta elemendi tekitamiseks JavaScripti.
// Vihje: background: darkblue, element.innerText,
// document.createElement(), document.body.appendChild()

const ex2Div = document.createElement('div')
ex2Div.innerText = 'Harjutus 2'
ex2Div.style.background = 'darkblue'
ex2Div.style.width = '250px'
ex2Div.style.height = '150px'
ex2Div.style.position = 'fixed'
ex2Div.style.top = '10px'
ex2Div.style.right = '20px'




document.body.appendChild(ex2Div)

// --------------------------------------------
// ----- Ülesanne 3 (element.addEventListener('click', function() {}))
// --------------------------------------------
// Tee nii, et ülesandes 2 tekitatud kast kaoks ekraanilt,
// kui sellel klikkida.

// --------------------------------------------
// ----- Ülesanne 4
// --------------------------------------------
// Kirjuta funktsioon, mis kuvaks ekraanil ülemises
// paremas nurgas sõnumikastikese etteantud tekstiga:
// Esimene parameeter on teksti, mida kuvada, teine on
// tõeväärtus, mis määrab, kas tegemist on veateatega
// või lihtsalt infoga.
// Kui info, kuva kastike rohelise taustaga; kui viga,
// siis kasuta punast tausta.
// Kui elemendil klikkida, tuleb see sulgeda.
