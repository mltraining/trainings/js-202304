// --------------------------------------------
// ----- Ülesanne 1
// --------------------------------------------
// Defineeri funktsioon submitForm() ja käivita see 
// kasutaja registreerimise vormi registreerimisnupule
// vajutamisel.
// Tee nii, et vaikimisi vormi saatmise protsess ei käivituks
// Vihje: event.preventDefault()

function submitForm(event) {
    event.preventDefault() // Ära refreshi lehekülge

    if (isFormValid()) {
        console.log('Form saadeti teele', event)
    } 
}

document.querySelector('form').addEventListener('submit', submitForm)

// --------------------------------------------
// ----- Ülesanne 2
// --------------------------------------------
// Pane maksuresidentsuse dropdown-ile külge
// event handler, mis muudaks Eesti isikukoodi
// sisestamise tekstikasti aktiivseks vaid juhul, 
// kui valitud on "Eesti". Kõigi muude valikute
// korral peab teksikast olema mitteaktiivne (disabled)
// ja tühi.

// addEventListener('change' või 'click')
// inputELement.removeAttribute('disabled')
// formElement.value = ''

const taxResidencyDropdown = document.querySelector('#tax-residency')
const estPin = document.querySelector('#est-pin')

taxResidencyDropdown.addEventListener('change', changeEstPinInputEnabledStatus)

function changeEstPinInputEnabledStatus(e) {
    const taxResidenceValue = e.target.value

    if (taxResidenceValue === 'est') {
        estPin.removeAttribute('disabled') 
    } else {
        estPin.setAttribute('disabled', true)
        estPin.value = ''
    }

    // console.log(taxResidenceValue)

    // estPin.removeAttribute('disabled')
    // estPin.setAttribute('disabled', true)

}

// taxResidencyDropdown.value


// --------------------------------------------
// ----- Ülesanne 3
// --------------------------------------------
// Vormil on checkbox "Olen külastanud välisriike viimase aasta jooksul".
// Tee nii, et kui see checkbox on märgistatud, siis kuvatakse
// hetkel varjatud div-element, mille sees textarea element.
// Vihjed: event.target.checked, display: none|flex

//  visitedCountriesElement.style.display = 'none'
const foreignCountriesCheckbox = document.querySelector('#foreign-countries')
const visitedCountriesListRow = document.querySelector('#list-visited-countries-row')
const visitedCountriesTextarea = document.querySelector('#list-visited-countries')

const changeForeignCountriesRowDisplay = e => {
    const checkboxChecked = e.target.checked
    if (checkboxChecked) {
        visitedCountriesListRow.style.display = 'flex'
    } else {
        visitedCountriesListRow.style.display = 'none'
        visitedCountriesTextarea.value = ''
    }
}

foreignCountriesCheckbox.addEventListener('click', changeForeignCountriesRowDisplay)

// Kompaktsem lahendus
// document.querySelector('#foreign-countries').addEventListener('click', e => e.target.checked 
//     ? document.querySelector('#list-visited-countries-row').style.display = 'flex'
//     : document.querySelector('#list-visited-countries-row').style.display = 'none')

// --------------------------------------------
// ----- Ülesanne 4
// --------------------------------------------
// Defineeri funktsioon isFormValid() ja käivita see
// vormi saatmisnupu vajutusel.
// Tee nii, et kui vormil ilmnevad puudused, siis vormi
// saatmine katkestatakse ja kuvatakse veateade.
// Kasuta veateate kuvamiseks alert('Lorem ipsum ipsum dolore') 
// funktsiooni.
// Defineeri funktsioonis järgmised kontrollid:
// 1) Eesnimi peab olema täidetud ja väjemalt kaks tähemärki pikk
// 2) Perenimi peab olema täidetud ja vähemalt kaks tähemärki pikk
//    Vihjed: document.querySelector(<SELECTOR>).value, "Bruno".length
// 3) Kasutaja peab olema täisealine (aasta täpsusega)
//    Vihjed: "2004-07-01".substr(0, 4), parseInt()
// 4) Maksuresidentsus peab olema valitud.
// 5) Kui kasutaja on Eesti maksuresident, peab olema täidetud isikukoodi lahter.
// 6) Kui kasutaja on viimase aasta jooksul külastanud välisriike, peavad need olema deklareeritud.
// 7) Kui kasutaja on valinud tagasihelistamise algusaja, peab ka lõppaeg olema määratud.
// 8) Tagasihelistamise algusaeg peab olema väiksem, kui lõppaeg.

function isFormValid() {
    const firstName = document.querySelector('#first-name')?.value
    const dateOfBirth = document.querySelector('#date-of-birth').value
    const callFrom = document.querySelector('#call-from').value
    const callUntil = document.querySelector('#call-until').value
    if (!firstName || firstName.length < 2) {
        // "tere".length
        displayMessageBox('Eesnimi määramata või liiga lühike!', true)
        return false
    } else if (dateOfBirth.length < 10 || new Date().getFullYear() - parseInt(dateOfBirth.substr(0, 4)) < 18) {
        displayMessageBox('Alaealisetel registreerimine keelatud!', true)
        return false
    } else if (callFrom.length === 0 && callUntil.length > 0 ||
        callFrom.length > 0 && callUntil.length === 0) {
        displayMessageBox('Kõne algus- ja lõppaeg peavad olema mõlemad kas määratud või määramata!', true)  
        return false
    } else if (callFrom.length > 0 && callUntil.length > 0 && callUntil <= callFrom) {
        displayMessageBox('Kõne lõpuaeg peab olema hilisem algusajast!', true)  
        return false
    }
    
    return true
}

// --------------------------------------------
// ----- Ülesanne 5
// --------------------------------------------
// Kui tagasihelistamise ajavahemik on määramata,
// küsi kliendilt enne vormi saatmist kinnitust, 
// et kas ta ikka soovib saata andmed ilma ajavahemikuta.
// Kui jah, siis "saada vorm", kui mitte, katkesta "saatmine".
// Vihje: confirm('Lorem ipsum ipsum')

// --------------------------------------------
// ----- Ülesanne 6
// --------------------------------------------
// Asenda alert() funktsiooni väljakutsumised
// meie oma sõnumikastikestega ning
// confirm() funktsiooni väljakutse meie oma
// confirm-modaliga.

// --------------------------------------------
// ----- Ülesanne 7
// --------------------------------------------
// Asenda valideerimisel kasutatavad sõnumikastikesed
// veateadete nimistuga, mis kuvatakse vormi alguses.

// --------------------------------------------
// ----- Ülesanne 8
// --------------------------------------------
// Täienda ülesande 4 punkt 3 valideerimistingimust:
// kasutaja peab olema täisealine päevatäpsusega.