let messageContainerElement = null

function displayMessageBox(message, isError = false) {
  addMessageBox(message, isError)
  if (isError) {
    return
  }
  setTimeout(() => removeMessageBox(), 5000)
}

function addMessageBox(message, isError) {
  if (messageContainerElement !== null) {
    return
  }
  messageContainerElement = document.createElement('div')
  messageContainerElement.innerHTML = isError
    ? getErrorMessageTemplate(message)
    : getSuccessMessageTemplate(message)
  document.body.appendChild(messageContainerElement).focus()
  document.querySelector('.message-box').style.opacity = '1'
  document.querySelector('.message-box').style.background = isError
    ? '#DF2E38'
    : '#5D9C59'
}

function removeMessageBox() {
  if (messageContainerElement) {
    document.body.removeChild(messageContainerElement)
    messageContainerElement = null
  }
}

const getSuccessMessageTemplate = (message) => /*html*/ `
      <div class="message-box">
        ${message}
      </div>
  `

const getErrorMessageTemplate = (message) => /*html*/ `
      <div class="message-box error-row">
        <div class="error-message-area">
          ${message}
        </div>
        <div class="error-x-area">
          <div class="x-button" onClick="removeMessageBox()">X</div>
        </div>
      </div>
  `
